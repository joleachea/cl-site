;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-site)

(defun make-site (&optional output-dir) 
  (let ((*output-dir* (or output-dir *output-dir*)))
    (format t "Generating site in ~A.~%" *output-dir*)
    (process-static)
    (process-styles)
    (process-pages)))
