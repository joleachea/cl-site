$(document).ready(function(){
  
  $("span.menu-link").click(function(event){
    var nextMenu = $(this).next(".menu");
    console.log(nextMenu.siblings(".menu").slideUp());
    nextMenu.siblings(".menu").slideUp();
    nextMenu.slideToggle();
  });

  /* Random Lisp quote in footer */

  var $quotes = $('#quotes > div');
  var $quote = $($quotes[Math.floor(Math.random() * $quotes.length)]);
  
  $('#lisp-quote').html($quote.html());
});
