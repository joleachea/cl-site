;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-user)

(asdf:defsystem cl-site
  :name "cl-site"
  :version "0.0.1"
  :maintainer "C. Yang"
  :author "C. Yang"
  :licence "TBD"
  :description "Static site generator for CLnet, written in CL"

  :depends-on (:cl-mustache)

  :components ((:module source
                        :pathname ""
                        :serial t
                        :depends-on (package)
                        :components ((:file "globals")
                                     (:file "helpers")
                                     (:file "process")
                                     (:file "main")))
               (:module package
                        :pathname ""
                        :components ((:file "package"))))
  :in-order-to ((asdf:test-op (asdf:test-op cl-site/t))))

(asdf:defsystem cl-site/t
  :defsystem-depends-on (prove-asdf)
  :depends-on (cl-site prove)
  :components ((:module site
                        :pathname "t/"
                        :components ((:test-file "site"))))
  :perform (asdf:test-op (op c)
                         (uiop:symbol-call :prove-asdf 'run-test-system c)))


