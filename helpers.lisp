;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-site)

; Makes pathname from prefix + page object
(defun make-path (prefix suffix &optional page)
  (let ((path (if page (cdr (assoc :content suffix)) suffix)))
    (if (pathnamep prefix)
        (merge-pathnames path prefix)
        (pathname (concatenate 'string 
                               prefix path)))))

; Slurps file from a string
(defun file-to-string (filepath)
  (with-open-file (stream filepath)
    (let ((data (make-string (file-length stream))))
      (read-sequence data stream)
      data)))