;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-site)

(defparameter *LAYOUT-DIR* (asdf:system-relative-pathname :cl-site "layout/templates/")
  "Pathname for location of template files.")

(defparameter *OUTPUT-DIR* (asdf:system-relative-pathname :cl-site "output/")
  "Pathname where the generated output should be written to.")

;; @TO-DO: store layout name as global, generate js/css pathnames from it. 
;; (More template-related variables may go here in the future)
(defparameter *DEFAULT-PAGE-TEMPLATE* "layout_2018.html"
  "Default template to use unless another is explicitly specified.")

;; @TO-DO: replace this with better css preprocessing.
(defparameter *STYLES-DIR* (asdf:system-relative-pathname :cl-site "layout/css/")
  "Pathname for location and list of styles.")
(defparameter *STYLES* 
  (list "layout_2018.css")
  "A list of strings representing relative filenames.")

;; JS files - separate from static files, because they are processed differently
;; within the template.
(defparameter *SCRIPTS-DIR* (asdf:system-relative-pathname :cl-site "layout/js/")
  "Pathname for location and list of scripts.")
(defparameter *SCRIPTS*
  (list "scripts.js")
  "A list of strings representing relative filenames.")

;; General static files - images etc
(defparameter *STATIC-DIR* (asdf:system-relative-pathname :cl-site "layout/static/")
  "Pathname for location of static files.")

;; The only REQUIRED field is :content which must be the filename of the page content
;; The :content field will NOT be sent to the template.
;; @TO-DO: store fields within content files and parse them out.
(defparameter *PAGES-DIR* (asdf:system-relative-pathname :cl-site "content/")
  "Pathname for location of page content.")
(defparameter *PRIVATE-KEYS* '(:slug :content))

;; Extra context for pages
(defparameter *PAGE-CONTEXT*
  `(("about.html" ((:title . "Common-Lisp.net | About")))
    ("index.html" ((:title . "Welcome to Common-Lisp.net!")))
    ("faq.html" ((:title . "Frequently asked questions")))
    ("gitlab-migration-repository-mapping.html" ((:title . "GitLab migration")))
    ("gitlab-migration-status.html" ((:title . "Migration to GitLab")))
    ("independent-lists.html" ((:title . "Independent Mailing Lists")))
    ("orphaned-mailing-lists.html" ((:title . "Orphaned mailing lists")))
    ("orphaned-projects.html" ((:title . "Orphaned projects")))
    ("phub.html" ((:title . "Projects")))
    ("project-intro.html" ((:title . "Project Hosting")))))

(defun page-context (filename)
  (cadr (assoc filename *page-context* :test 'equalp)))

(defun page-title (filename)
  (or (cdr (assoc :title (page-context filename)))
      (format nil "Common-Lisp.net | ~a"
              (string-capitalize
               (pathname-name (pathname filename))))))

(defparameter *PAGES* 
  (mapcar (lambda (p)
            (let ((context
                   (append (list (cons :content (file-namestring p)))
                           (page-context (file-namestring p)))))
              (when (not (assoc :title context))
                (push (cons :title (page-title (file-namestring p)))
                      context))
              context))                         
	  (directory (make-pathname :defaults *PAGES-DIR*
				    :name :wild
				    :type "html")))
  "Each page is an alist containing info to be sent to the template via the context.")

;; Initialize global context (will be appended to all individual page contexts)
;; Used to store things like stylesheets, etc...
(defparameter *GLOBAL-CONTEXT* ())
