;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :cl-site)

;; Copies/renders stylesheets + adds to global context
(defun process-styles ()
  (let
      ((styles-list (mapcar
                     (lambda (s) (progn
                              (uiop:copy-file (make-path *STYLES-DIR* s)
                                              (make-path *OUTPUT-DIR* s))
                              (cons :location s)))
                     *STYLES*)))
    (push (cons :styles styles-list) *GLOBAL-CONTEXT*)))

;; Copies/renders script files + adds to global context
(defun process-scripts () nil)

;; Copies/renders static assets (imgs, etc)
(defun process-static ())

#+nil
(defun process-static ()
  (let*
      ((static-output-dir (merge-pathnames #P"static/" *OUTPUT-DIR*))
       (static-list (mapcar
                     (lambda (s) (progn
                              (uiop:copy-file (make-path *STATIC-DIR* s)
                                              (make-path static-output-dir s))
                              (cons :location s)))
                     (mapcar
                      (lambda (p) (file-namestring p))
                      (directory (make-pathname :defaults *STATIC-DIR*
                                                :name :wild
                                                :type :wild))))))
    (push (cons :static static-list) *GLOBAL-CONTEXT*)))

;; process-pages : Copies/renders pages - called last


(defun render-page (page template-path)
  (let*
      ((page-path (make-path *PAGES-DIR* page t))
       (output-path (make-path *OUTPUT-DIR* page t))
       (page-content (file-to-string page-path))
       (page-context (remove-if (lambda (p) (find p *PRIVATE-KEYS*))
                                (push (cons :page-content page-content) page) :key 'car)))

    (with-open-file (output-stream output-path
                                   :direction :output
                                   :if-exists :supersede)
      (mustache:render template-path (append page-context *GLOBAL-CONTEXT*)
                       output-stream))))

(defun process-pages ()
  (format t "Processing pages..~%")
  (let ((template-path (pathname (merge-pathnames *LAYOUT-DIR* *DEFAULT-PAGE-TEMPLATE*))))
    (ensure-directories-exist *OUTPUT-DIR*)
    (loop for page in *PAGES*
       do
         (format t "Generating ~A~%" (cdr (assoc :content page)))
         (render-page page template-path))
    (format t "Done.~%")))
