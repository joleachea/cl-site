# Common-Lisp.net site generator #

## Dependencies

CL-MUSTACHE for HTML templating

## Execution

### From command line (recommended) ###

Using Make.

The Makefile contains various useful commands. 

Among them:

* `make` - to generate the site.
* `make serve` - to run a simple site server (needs python)
* `make clean` - to remove the generated files from output directory

### From Lisp ###

Load with this command:

    (ql:quickload :cl-site)

To generate the site: 

    (cl-site::make-site)

The output files will be generated in the `output/` directory.
