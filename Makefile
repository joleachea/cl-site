all: build

serve:
	cd output; python ../server.py

static:
	mkdir -p output/static
	cp -rf content/static/* output/static

favicon:
	cp content/favicon.ico output/

build: static favicon
	sbcl --load build.lisp --non-interactive

clean:
	rm -rf output
	mkdir output
